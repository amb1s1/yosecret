from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'secrets.views.home', name='home'),
    url(r'^stream/', 'stream.views.stream', name='stream'),
    url(r'^secret/(?P<slug>.*)/', 'secrets.views.secret', name='secret'),
    url(r'^sender/(?P<slug>.*)/', 'secrets.views.sender', name='sender'),
    url(r'^callback/', 'secrets.views.callback', name='callback'),
    url(r'^passcode/', 'secrets.views.passcode', name='passcode'),
    url(r'^messenger/(?P<youser>.*)', 'secrets.views.messenger', name='messenger'),
    url(r'^subs/', 'secrets.views.subs', name='subs'),
    url(r'^admin/', include(admin.site.urls)),
)
