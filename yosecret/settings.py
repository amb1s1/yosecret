"""
Django settings for yosecret project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
MY_MODULE = os.path.join((BASE_DIR), "modules")
sys.path.append(MY_MODULE)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'gp3jfh6fmp&o^0_baz_qe#96(^j)xf)n2%pizl&n42i%@#t1n!'

# SECURITY WARNING: don't run with debug turned on in production!
import socket
servername = socket.gethostname()
DEBUG = False
if servername == "mint":
    DEBUG = True
if servername == "localhost":
    DEBUG = True
TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'crispy_forms',
    'secrets',
    'stream',
)

CRISPY_TEMPLATE_PACK = 'bootstrap3'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'yosecret.urls'

WSGI_APPLICATION = 'yosecret.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'mydatabase',
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
TEMPLATE_DIRS = (
    os.path.join((BASE_DIR), "static","templates"),
    )

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join((BASE_DIR), "static","media")
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join((BASE_DIR), "static","root")
STATICFILES_DIRS = (
    os.path.join((BASE_DIR), "static","static"),
) 

if not DEBUG:
    ###HEROKU SETTINGS
    # Parse database configuration from $DATABASE_URL
    import dj_database_url
    DATABASES['default'] =  dj_database_url.config()

    # Honor the 'X-Forwarded-Proto' header for request.is_secure()
    SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

    # Allow all host headers
    ALLOWED_HOSTS = ['*']

    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join((BASE_DIR), "static","media")
    STATIC_URL = '/static/'
    STATIC_ROOT = os.path.join((BASE_DIR), "static","root")
    STATICFILES_DIRS = (
        os.path.join((BASE_DIR), "static","static"),
    )
