from django.db import models
from django.contrib.auth.models import User

class Profile(models.Model):
	user_profile = models.CharField(max_length=100, unique=True)
	active = models.BooleanField(default=True)
	yoverify = models.BooleanField(default=True)
	user_profile_today_id = models.CharField(max_length=100, null=True, blank=True)
	user_profile_today_id_date = models.CharField(max_length=12, null=True, blank=True)


	def __unicode__ (self):
		return self.user_profile

# Create your models here.
class Secret(models.Model):
	fromyo = models.ForeignKey(Profile, related_name='from')
	to = models.ForeignKey(Profile, related_name='to')
	secretmessage =	models.TextField(max_length=200)
	expiredtime = models.IntegerField(default=86400)
	readexpiredtime = models.IntegerField(default=10)
	slug = models.SlugField(unique=True, )
	passcode = models.CharField(max_length=4, blank=True, null=True)
	yosecetadmire = models.BooleanField(default=False)
	isread = models.BooleanField(default=False)
	canbedelete = models.BooleanField(default=True)
	isreadcount = models.IntegerField(default=0)

	def __unicode__ (self):
		return '%s' % (self.fromyo) 

class Follow(models.Model):
	user = models.ForeignKey(Profile, related_name='user')
	followed_user = models.ForeignKey(Profile, related_name='followed_user')
	conv_passcode = models.CharField(max_length=4, blank=True, null=True)

	def __unicode__ (self):
		return '%s' % (self.user)
