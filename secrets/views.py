"""This script is for having all the views"""
from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from .forms import SendForm, PassForm, SubsForm, ReplyForm
from secrets.models import Secret, Profile, Follow
import yopy
import string
import random
import datetime

#This is for disabling yo service
SENDYODEBUG = False
# YO API ID number
yo_api = '3d64910f-2a9c-47f2-8805-b2f5bf1a2588'


def id_generator(size=30, chars=string.ascii_uppercase + string.digits, isslug=True):
    # Create your views here.
    # This will create an unique slug ID
    if isslug:
        slugresult = True

        while slugresult == True:
            slug = ''.join(random.choice(chars) for _ in range(size))
            try:
                obj = Secret.objects.get(slug=slug)
            except Secret.DoesNotExist:
                slugresult = False
                return slug
    else:
        slugresult = True
        while slugresult == True:
            slug = ''.join(random.choice(chars) for _ in range(size))
            try:
                obj = Profile.objects.get(user_profile_today_id=slug)
            except Profile.DoesNotExist:
                slugresult = False
                return slug

def changepasscode(secret, passcode):
    chpwd = Follow.objects.get(user=secret.fromyo.pk, followed_user=secret.to.pk)
    chpwd1 = Follow.objects.get(user=secret.to.pk, followed_user=secret.fromyo.pk)
    chpwd.conv_passcode = passcode
    chpwd1.conv_passcode = passcode
    chpwd.save()
    chpwd1.save()
def createfriendship(fromyoobj, toobj, passcode=None):
    # This function is to know if a message was delete or not. If message was delete
    # sender will see a message saying that the message was read
    # For the receiver, the message will say that the message is not available
    if not Follow.objects.filter(user=fromyoobj.pk, followed_user=toobj.pk).exists():
        Follow.objects.create(
            user=fromyoobj, followed_user=toobj, conv_passcode=passcode)
    if not Follow.objects.filter(user=toobj.pk, followed_user=fromyoobj.pk).exists():
        Follow.objects.create(
            user=toobj, followed_user=fromyoobj, conv_passcode=passcode)


def message(slug, sender):
    if not sender:
        secret = Secret.objects.get(slug=slug)
        secret.save()
        return secret
    else:
        secret = Secret.objects.get(slug=slug)
        return secret


def deletemessage(slug, sender, auth=False):
    if sender:
        try:
            secret = Secret.objects.get(slug=slug)
            follow = Follow.objects.get(user=secret.fromyo.pk, followed_user=
                secret.to.pk)
        except:
            pass
    else:
        try:
            secret = Secret.objects.get(slug=slug)
        except:
            pass
        if secret.canbedelete:
            follow = Follow.objects.get(user=secret.fromyo.pk, followed_user=
                secret.to.pk)
            if follow.conv_passcode:
                if auth:
                    secret.isread = True
                    secret.isreadcount +=2
                    secret.save()
                else:
                    pass
            else:
                secret.isread = True
                secret.isreadcount +=1
                secret.save()
        else:
            pass
    return secret, follow


def verifyuser(youser):
    yo = yopy.Yo(yo_api)
    try:
        yo.youser(youser)
        return True
    except:
        return False


def writedata(form, secret, sender):
    # This funtion will validate the form and will send a YO
    if form.is_valid():
        slug = id_generator()
        secretmessage = form.cleaned_data['secretmessage']
        passcode = form.cleaned_data['passcode']
        if sender:
            Secret.objects.create(
                fromyo=secret.fromyo, to=secret.to, secretmessage=secretmessage, passcode=None, slug=slug)
        else:
            Secret.objects.create(
                fromyo=secret.to, to=secret.fromyo, secretmessage=secretmessage, passcode=None, slug=slug)

        return slug, secret


def sendyo(to, slug):
    # This while will make sure that it gets an unique slug
    # This is for debug purpose
    if not SENDYODEBUG:
        yo = yopy.Yo(yo_api)
        url_link = 'http://yosecret.herokuapp.com/secret/' + slug
        yo.youser(to, url_link)
    else:
        pass


def home(request):
    # This is the fuction for the HOME PAGE
    # if request.method == "POST":
    #     form = SubsForm(request.POST)
    #     if form.is_valid():
    #         user = form.cleaned_data['user_profile']
    #         yoverify = verifyuser(user)
    #         # obj = Profile(user_profile = user)
    #         # obj.save()
    #     return HttpResponseRedirect('/')

    # else:
    #     form = SubsForm()
    context = locals()
    template = 'home.html'
    return render_to_response(template, context, RequestContext(request))


def stream(request):
    # This funtion is for all public available messages
    h = Secrets.object.all()
    context = locals()
    template = 'stream.html'
    return render_to_response(template, context)


def secret(request, slug):

    if request.method == "POST":
        form = ReplyForm(request.POST)
        if form.is_valid():
            passcode = form.cleaned_data['passcode']
        secret = Secret.objects.get(slug=slug)
        slug, secret = writedata(form, secret, sender=False)
        to = secret.fromyo.user_profile
        if passcode:
            changepasscode(secret, passcode)
        sendyo(to, slug)
        return HttpResponseRedirect("/sender/" + str(slug))

    else:
        secret = message(slug, sender=False)
        secret, follow = deletemessage(slug, sender=False)
        form = ReplyForm()
        passform = PassForm()

    context = {'form': form, 'passform': passform,
               'secret': secret, 'follow': follow}
    template = 'secret.html'
    return render_to_response(template, context, RequestContext(request))


def callback(request):
    # Callback receive when user tap the YOSECRET on their phone
    # This will grab the username from a GET
    if request.method == "GET":
        callback = request.GET

        callback = callback["username"]

        context = {'callback': callback}
        if not Profile.objects.filter(user_profile=callback).exists():
            today = datetime.date.today()
            random_id = id_generator(size=10, isslug=False)
            Profile.objects.create(
                user_profile=callback.upper(), user_profile_today_id=random_id, user_profile_today_id_date=today.strftime("%m%d%Y"))
        yo = yopy.Yo(yo_api)
        url_link = 'http://yosecret.herokuapp.com/messenger/' + callback
        yo.youser(callback, url_link)
        template = 'messenger.html'
        return render(request, template, context)


def messenger(request, youser):
    # This is the function that it use when YOSECRET service send a YO to the user
    # user will hit this funtion
    form = SendForm(request.POST or None, initial={'fromyo': youser})
    slug = id_generator()
    # To grab the form data
    if form.is_valid():
        fromyo = form.cleaned_data['fromyo']
        to = form.cleaned_data['to']
        secretmessage = form.cleaned_data['secretmessage']
        passcode = form.cleaned_data['passcode']
        # Find out if the user is the database
        try:
            fromyoobj = Profile.objects.get(user_profile=fromyo.upper())
        except:
            today = datetime.date.today()
            random_id = id_generator(size=10, isslug=False)
            Profile.objects.create(
                user_profile=fromyo.upper(), user_profile_today_id=random_id, user_profile_today_id_date=today.strftime("%m%d%Y"))
            fromyoobj = Profile.objects.get(user_profile=fromyo.upper())
        try:
            toobj = Profile.objects.get(user_profile=to.upper())
        except:
            today = datetime.date.today()
            random_id = id_generator(size=10, isslug=False)
            Profile.objects.create(
                user_profile=to.upper(), user_profile_today_id=random_id, user_profile_today_id_date=today.strftime("%m%d%Y"))
            toobj = Profile.objects.get(user_profile=to.upper())
        Secret.objects.create(
            fromyo=fromyoobj, to=toobj, secretmessage=secretmessage, passcode=None, slug=slug)
        createfriendship(fromyoobj, toobj, passcode)
        sendyo(to, slug)
        return HttpResponseRedirect("/sender/" + slug)
    context = {'form': form}
    template = 'messenger.html'
    return render(request, template, context)


def sender(request, slug):
    # This funtion is for the sender to view current send messages
    secret, follow = deletemessage(slug, sender=True)
    form = ReplyForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            passcode = form.cleaned_data['passcode']
        if passcode:
            changepasscode(secret, passcode)
        slug, to = writedata(form, secret, sender=True)
        to = to.to.user_profile
        sendyo(to, slug)
        return HttpResponseRedirect('/sender/' + str(slug))
    context = {'secret': secret, 'form': form, 'follow':follow}
    template = 'sender.html'
    return render(request, template, context)


def passcode(request):

    if request.method == "POST":
        passcode = request.POST["passcode"]
        slug = request.POST["slug"]
        secret, follow = deletemessage(slug, sender=False)
        if follow.conv_passcode:

            if str(passcode) == str(follow.conv_passcode):
                auth = True
                secret, follow = deletemessage(slug, sender=False, auth=True)
            else:
                auth = False
    else:
        passcode = ""
    context = locals()
    template = 'passcode.html'
    return render_to_response(template, context, RequestContext(request))


def subs(request):

    if request.method == "POST":
        user = request.POST["user"]
        yoverify = verifyuser(user)
        if yoverify:
            obj = Profile.objects.filter(user_profile="amb1s1").exists()
            if obj:
                message = "Thank for your subscription, but keep it a secret."
                message1 = "To access our messenger, click the button on the bottom or tap YOSECRET on your YO APP. Yo!!!!"
            else:
                obj = Profile(user_profile=user)
                obj.save()
                message = "Thanks for your subscription, but keep it a secret."
        else:
            message = "Use your real YO Name, remember we will not show your YO Name to the person that you sent a YO SECRET"

    else:
        user = ""
    context = locals()
    template = 'subs.html'
    return render_to_response(template, context, RequestContext(request))
