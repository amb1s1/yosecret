from django.contrib import admin
from models import *
# Register your models here.


class SecretAdmin(admin.ModelAdmin):
    list_display = ('fromyo', 'to', 'secretmessage')

    class Meta:
        model = Secret


class ProfileAdmin(admin.ModelAdmin):

    class Meta:
        model = Profile


class FollowAdmin(admin.ModelAdmin):
    list_display = ('user', 'followed_user')

    class Meta:
        model = Follow


admin.site.register(Profile, ProfileAdmin)
admin.site.register(Follow, FollowAdmin)
admin.site.register(Secret, SecretAdmin)
