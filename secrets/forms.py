from django.forms import ModelForm, TextInput
from django.forms import Textarea
from django import forms
from models import *
import yopy


yo_api = '3d64910f-2a9c-47f2-8805-b2f5bf1a2588'


def verifyuser(youser):
    yo = yopy.Yo(yo_api)
    try:
        yo.youser(youser)
        return True
    except:
        return False


class SubsForm(ModelForm):

    class Meta:
        model = Profile

        fields = ['user_profile']
class ReplyForm(forms.Form):
    secretmessage = forms.CharField(
        widget=forms.Textarea(attrs={'rows': '6', 'placeholder': 'Secret Messages', 'maxlength' : '200'}))
    passcode = forms.CharField(widget=forms.TextInput(
        attrs={'type':'password','placeholder': 'Optional - Create or Change Your Four Digits Passcode'}), required=False)

class SendForm(forms.Form):
    fromyo = forms.CharField()
    to = forms.CharField()
    secretmessage = forms.CharField(
        widget=forms.Textarea(attrs={'rows': '6', 'placeholder': 'Secret Messages'}))
    passcode = forms.CharField(widget=forms.TextInput(
        attrs={'type':'password','placeholder': 'Optional - Create or Change Your Four Digits Passcode'}), required=False)

    def clean(self):

        cleaned_data = super(SendForm, self).clean()
        fromyo = self.cleaned_data.get("fromyo")
        to = self.cleaned_data.get("to")
        secretmessage = cleaned_data.get("secretmessage")
        passcode = cleaned_data.get("passcode")
        
        try:

            fromyoobj = Profile.objects.get(user_profile=fromyo.upper())
            fromverify = fromyoobj.yoverify
            print fromverify
            
            
        except:
            
            fromverify = False

        try:
            toobj = Profile.objects.get(user_profile=to.upper())
            toverify = toobj.yoverify
        except:
            toverify = False

        if not fromverify:
            fromverify = verifyuser(fromyo)
        if not toverify:
            toverify = verifyuser(to)
        if not fromverify:
            self._errors['fromyo'] = self.error_class(
                ["Please type a real YO USERNAME"])
        if not toverify:
            self._errors['to'] = self.error_class(
                ["Please type a real YO USERNAME"])
        return cleaned_data


class PassForm(ModelForm):

    class Meta:
        model = Secret

        fields = ['passcode']
        widgets = {

            'passcode': TextInput(attrs={'type':'password','placeholder': 'Four Digits Passcode'}),
        }
