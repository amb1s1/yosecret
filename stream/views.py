from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from secrets.models import Secret 


# Create your views here.
def stream(request):
	secrets = Secret.objects.all()
	context 	= 	locals()
	template	=	'stream.html'
	return render_to_response(template, context)